package br.com.qpainformatica.nfpsolidaria.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.com.qpainformatica.nfpsolidaria.model.entity.FileInfo;
import br.com.qpainformatica.nfpsolidaria.model.service.NotaService;

@Controller
public class FileuploadController {
	@Autowired
	ServletContext context;

	private NotaService ns;

	@Autowired
	public FileuploadController(NotaService ns) {
		this.ns = ns;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity upload(@RequestParam("file") List<MultipartFile> files) {
		List<FileInfo> uploadedFiles = new ArrayList<FileInfo>();

		if (!files.isEmpty()) {
			try {
				for (MultipartFile file : files) {
					String path = context.getRealPath("/WEB-INF/uploaded") + File.separator
							+ file.getOriginalFilename();
					System.out.println("UPLOAD-->" + path);
					File destinationFile = new File(path);
					file.transferTo(destinationFile);
					uploadedFiles.add(new FileInfo(destinationFile.getName(), path));
				}
				return new ResponseEntity(HttpStatus.OK);

			} catch (Exception e) {
				System.out.println("UPLOAD--> ERRO NO UPLOAD: ");
				System.out.println(e.getMessage());
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}

		}
		return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = "/uploaded/{image}")
	@ResponseBody
	public byte[] getImage(@PathVariable String image, HttpServletRequest request) {
		String rpath = request.getRealPath("/");
		System.out.println("SERVLETPATH=" + rpath);
		rpath = rpath + "WEB-INF/uploaded/" + image + ".jpg"; // whatever path
																// you used for
																// storing the
																// file
		Path path = Paths.get(rpath);
		System.out.println("REQUESTPATH=" + path);
		byte[] data = null;
		try {
			data = Files.readAllBytes(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	@Transactional
	@RequestMapping(method = RequestMethod.DELETE, value = "/rest/delete/{image}")
	public ResponseEntity<String> deleteImage(@PathVariable String image, HttpServletRequest request) {
		String rpath = request.getRealPath("/");
		System.out.println("SERVLETPATH=" + rpath);
		rpath = rpath + "WEB-INF/uploaded/" + image + ".jpg"; // whatever path
																// you used for
																// storing the
																// file
		Path path = Paths.get(rpath);
		System.out.println("REQUESTPATH=" + path);

		try {
			Files.delete(path);
			return new ResponseEntity<String>("Deletado com sucesso.", HttpStatus.OK);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>("Falha ao tentar deletar.", HttpStatus.BAD_REQUEST);
		}

	}

//	@RequestMapping(value = "/getimage/{id}")
//	public ResponseEntity<byte[]> getImageById(@PathVariable int id, HttpServletRequest request,HttpServletResponse response) {
//		String rpath = request.getRealPath("/");
//		
//		
//		String image = ns.getImageNameById(id);
//																
//	    HttpHeaders headers = new HttpHeaders();
//	    InputStream in = request.getServletContext().getResourceAsStream("/WEB-INF/uploaded/" + image + ".jpg");
//	    byte[] media=null;
//		try {
//			media = IOUtils.toByteArray(in);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    headers.setCacheControl(CacheControl.noCache().getHeaderValue());
//	     
//	    ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
//	    return responseEntity;
//	}
	
	@RequestMapping(value = "/getimage/{id}", method = RequestMethod.GET)
	public void getImageAsByteArray(@PathVariable("id") Integer id,HttpServletRequest request,HttpServletResponse response) throws IOException {
	    
		String image = ns.getImageNameById(id);
		
		InputStream in = request.getServletContext().getResourceAsStream("/WEB-INF/uploaded/" + image + ".jpg");
	    response.setContentType(MediaType.IMAGE_JPEG_VALUE);
	    IOUtils.copy(in, response.getOutputStream());
	}

}