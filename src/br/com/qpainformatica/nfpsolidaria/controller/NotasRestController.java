package br.com.qpainformatica.nfpsolidaria.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.qpainformatica.nfpsolidaria.model.entity.Empresa;
import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;
import br.com.qpainformatica.nfpsolidaria.model.entity.User;
import br.com.qpainformatica.nfpsolidaria.model.entity.View;
import br.com.qpainformatica.nfpsolidaria.model.service.EmpresaService;
import br.com.qpainformatica.nfpsolidaria.model.service.NotaService;
import br.com.qpainformatica.nfpsolidaria.model.service.UserService;

@RestController
public class NotasRestController {


	private UserService us;
	private NotaService ns;
	private EmpresaService es;

	@Autowired
	public NotasRestController( UserService us, NotaService ns, EmpresaService es) {
		this.us=us;
		this.ns = ns;
		this.es = es;
	}
	
	
	// Usuário Envio de notas
	
	@Transactional
	@JsonView(View.Summary.class)
	@RequestMapping(method = RequestMethod.POST, value = "rest/envio")
	public @ResponseBody List<Nota> listarUsuariosNotas(@RequestBody User user){
		List<Nota> userreturn=null;
		
		System.out.println("USER AQUI NESTE="+user.toString());
		
		userreturn = us.getNotas(user);
	
		return userreturn;
	}
	
	
	// NOTAS
	
	@Transactional
	@JsonView(View.Summary.class)
	@RequestMapping(method = RequestMethod.PUT, value = "rest/notas/alterar")
	public ResponseEntity<String> alterarNotas(@RequestBody List<Nota> notas){
			ns.alterarNotas(notas);
			return new ResponseEntity<String>("Alterado com sucesso.", HttpStatus.OK);
			
	}
	
	@Transactional
	@JsonView(View.Summary.class)
	@RequestMapping(method = RequestMethod.DELETE, value = "rest/notas/deletar")
	public ResponseEntity<String> deletarNotas(@RequestBody List<Nota> notas){
			ns.deletarNotas(notas);
			return new ResponseEntity<String>("Deletado com sucesso.", HttpStatus.OK);
	}
	
	@Transactional
	@JsonView(View.Summary.class)
	@RequestMapping(method = RequestMethod.DELETE, value = "rest/notas/deletar/id/{id}")
	public ResponseEntity<String> deletarNotaById(@PathVariable("id") int  id){
			ns.deletarNotaById(id);
			return new ResponseEntity<String>("Deletado com sucesso.", HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas")
	public @ResponseBody List<Nota> getAllNotas() {
		List<Nota> lista = null;
		lista = ns.getAllNotas();
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/empresa/{empresa}")
	public @ResponseBody List<Nota> getNotasByEmpresa(@PathVariable("empresa") Integer id) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (id>0) {
			lista = ns.getNotasByEmpresa(id);	
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/empresa/cnpj/{cnpj}")
	public @ResponseBody List<Nota> getNotasByEmpresaCNPJ(@PathVariable("cnpj") String cnpj) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (!cnpj.isEmpty()) {
			lista = ns.getNotasByEmpresaCNPJ(cnpj);	
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/empresa/{empresa}/cpf/{cpf}")
	public @ResponseBody List<Nota> getNotasByEmpresaCPF(@PathVariable("empresa") Integer id,@PathVariable("cpf") String cpf) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (id>0 && !cpf.isEmpty()) {
			lista = ns.getNotasByEmpresaCPF(id,cpf);	
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/maior/{data}")
	public @ResponseBody List<Nota> getAllNotasMaiorQueData(@PathVariable("data") String data) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (data!=null && !data.equals("")) {
			lista = ns.getAllNotasMaiorQueData(data);
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/id/maior/{id}")
	public @ResponseBody List<Nota> getAllNotasMaiorQueID(@PathVariable("id") int id) {
		List<Nota> lista = new ArrayList<Nota>();
		if (id>-1) {
			lista = ns.getAllNotasMaiorQueId(id);
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/id/{id}")
	public @ResponseBody Nota getNotaByID(@PathVariable("id") int id) {
		return ns.getNotaById(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/notas/empresa/{id}/maior/{data}")
	public @ResponseBody List<Nota> getAllNotasMaiorQueData(@PathVariable("id") Integer id,@PathVariable("data") String data) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (data!=null && !data.equals("") && id>0) {
			lista = ns.getNotaByEmpresaMaiorQueData(""+id,data);
		}
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/nota/referencia/{referencia}")
	public @ResponseBody List<Nota> getAllNotaByReferencia(@PathVariable("referencia") String referencia) {
		List<Nota> lista = new ArrayList<Nota>();;
		if (referencia !=null && !referencia.equals("")) {
			lista = ns.getNotaByReferencia(referencia);
		}
		return lista;
	}
	
	
	// USUARIO
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/usuarios")
	public @ResponseBody List<User> getAllUsuarios() {
		List<User> lista = null;
		lista = us.getAllUsers();
		return lista;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/usuario/{cpf}")
	public @ResponseBody User getUsuarioByCPF(@PathVariable("cpf") String cpf) {
		User user = null;
		if (cpf!=null && !cpf.equals("")) {
			user = us.getUserByCPF(cpf);	
		}
		return user;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/usuarios/maior/{data}")
	public @ResponseBody List<User> getAllUsuariosMaiorQue(@PathVariable("data") String data) {
		List<User> lista = null;
		if(!data.isEmpty()){
			lista = us.getAllUsersMaiorQue(data);
		}
		return lista;
	}
	

	// EMPRESA
	
	@Transactional
	@JsonView(View.Summary.class)
	@RequestMapping(method = RequestMethod.POST, value = "rest/empresa")
	public ResponseEntity<Void> criarEmpresa(@RequestBody Empresa empresa ) {

		int response = es.inserirAlterarEmpresa(empresa);
		switch(response){
		case 1:
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		case 2:
			return new ResponseEntity<Void>(HttpStatus.OK);
		}

		return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/empresa/codigo/{codigo}")
	public @ResponseBody List<Empresa> getEmpresaByCodigo(@PathVariable("codigo") int codigo) {
		List<Empresa> lista = new ArrayList<Empresa>();;
		if (codigo > 0) {
			lista = es.getEmpresasByCodigo(codigo);
		}
		return lista;
	}

	@RequestMapping(method = RequestMethod.GET, value = "rest/empresa/cnpj/{cnpj}")
	public @ResponseBody Empresa getEmpresaByCNPJ(@PathVariable("cnpj") String cnpj) {
		Empresa empresa = new Empresa();
		if (!cnpj.isEmpty()) {
			empresa = es.getEmpresaByCNPJ(cnpj);
		}
		return empresa;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "rest/empresa/maior/{data}")
	public @ResponseBody Empresa getEmpresaMaiorData(@PathVariable("data") String data) {
		Empresa empresa = null;
		if (!data.isEmpty()) {
			empresa = es.getEmpresaMaiorData(data);
		}
		return empresa;
	}

	
}
