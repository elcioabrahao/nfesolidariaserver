package br.com.qpainformatica.nfpsolidaria.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="nota")
public class Nota implements Serializable{

	@Id
	@Column(name="id_nota")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(View.Summary.class)
	private int id;
	@Column(name="arquivo")
	@JsonView(View.Summary.class)
	public String arquivo;
	@Column(name="referencia")
	@JsonView(View.Summary.class)
    public String referencia;
	@Column(name="data_envio")
	@JsonView(View.Summary.class)
    public String dataEnvio;
	@Column(name="data_credito")
	@JsonView(View.Summary.class)
    public String dataCredito;
	@Column(name="user_id")
	@JsonView(View.Summary.class)
    public String userId;
	@Column(name="status")
	@JsonView(View.Summary.class)
    public int status;
	@Column(name="valor")
	@JsonView(View.Summary.class)
    public long valor;
	@Column(name="empresa")
	@JsonView(View.Summary.class)
    public String empresa;
	@Column(name="data_referencia")
	@JsonView(View.Summary.class)
    public String dataReferencia;
	@Column(name="nota_numero")
	@JsonView(View.Summary.class)
    public long notaNumero;
	@Column(name="valor_doado")
	@JsonView(View.Summary.class)
    public long valorDoado;
	@Column(name="nota_nome")
	@JsonView(View.Summary.class)
    public String notaNome;
	@Column(name="nota_cnpj")
	@JsonView(View.Summary.class)
    public String notaCNPJ;
	@Column(name="nota_data")
	@JsonView(View.Summary.class)
    public String notaData;
	@Column(name="nota_descricao")
	@JsonView(View.Summary.class)
    public String notaDescricao;
	
	
	
	public Nota() {
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getArquivo() {
		return arquivo;
	}



	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}



	public String getReferencia() {
		return referencia;
	}



	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}



	public String getDataEnvio() {
		return dataEnvio;
	}



	public void setDataEnvio(String dataEnvio) {
		this.dataEnvio = dataEnvio;
	}



	public String getDataCredito() {
		return dataCredito;
	}



	public void setDataCredito(String dataCredito) {
		this.dataCredito = dataCredito;
	}



	public String getUserId() {
		return userId;
	}



	public void setUserId(String userId) {
		this.userId = userId;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public long getValor() {
		return valor;
	}



	public void setValor(long valor) {
		this.valor = valor;
	}



	public String getEmpresa() {
		return empresa;
	}



	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}



	public String getDataReferencia() {
		return dataReferencia;
	}



	public void setDataReferencia(String dataReferencia) {
		this.dataReferencia = dataReferencia;
	}



	public long getNotaNumero() {
		return notaNumero;
	}



	public void setNotaNumero(long notaNumero) {
		this.notaNumero = notaNumero;
	}



	public long getValorDoado() {
		return valorDoado;
	}



	public void setValorDoado(long valorDoado) {
		this.valorDoado = valorDoado;
	}



	public String getNotaNome() {
		return notaNome;
	}



	public void setNotaNome(String notaNome) {
		this.notaNome = notaNome;
	}



	public String getNotaCNPJ() {
		return notaCNPJ;
	}



	public void setNotaCNPJ(String notaCNPJ) {
		this.notaCNPJ = notaCNPJ;
	}



	public String getNotaData() {
		return notaData;
	}



	public void setNotaData(String notaData) {
		this.notaData = notaData;
	}



	public String getNotaDescricao() {
		return notaDescricao;
	}



	public void setNotaDescricao(String notaDescricao) {
		this.notaDescricao = notaDescricao;
	}



	@Override
	public String toString() {
		return "Nota [id=" + id + ", arquivo=" + arquivo + ", referencia=" + referencia + ", dataEnvio=" + dataEnvio
				+ ", dataCredito=" + dataCredito + ", userId=" + userId + ", status=" + status + ", valor=" + valor
				+ ", empresa=" + empresa + ", dataReferencia=" + dataReferencia + ", notaNumero=" + notaNumero
				+ ", valorDoado=" + valorDoado + ", notaNome=" + notaNome + ", notaCNPJ=" + notaCNPJ + ", notaData="
				+ notaData + ", notaDescricao=" + notaDescricao + "]";
	}

	
	

	
	
}
