package br.com.qpainformatica.nfpsolidaria.model.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author elcio
 *
 */
@Entity
@Table(name="user")
public class User implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_user")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(View.Summary.class)
	private int id;
	@Column(name="nome")
	@JsonView(View.Summary.class)
    private String nome;
	@Column(name="cpf")
	@JsonView(View.Summary.class)
    private String cpf;
	@Column(name="data_nascimento")
	@JsonView(View.Summary.class)
    private String dataNascimento;
	@Column(name="senha_sefaz")
	@JsonView(View.Summary.class)
    private String senhaSEFAZ;
	@Column(name="identificacao")
	@JsonView(View.Summary.class)
    private String identificacao;
	@Column(name="data_alteracao")
	@JsonView(View.Summary.class)
    private String dataAlteracao;	
	@JsonProperty("notas")
	@JsonView(View.Summary.class)
	@Transient
	private Nota[] notas;


	public User() {
	}
	
	
	
	@Transient
	public Nota[] getNotas() {
		return notas;
	}

	@Transient
	public void setNotas(Nota[] notas) {
		this.notas = notas;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSenhaSEFAZ() {
		return senhaSEFAZ;
	}

	public void setSenhaSEFAZ(String senhaSEFAZ) {
		this.senhaSEFAZ = senhaSEFAZ;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	public String getIdentificacao() {
		return identificacao;
	}



	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}



	public String getDataAlteracao() {
		return dataAlteracao;
	}



	public void setDataAlteracao(String dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", nome=" + nome + ", cpf=" + cpf + ", dataNascimento=" + dataNascimento
				+ ", senhaSEFAZ=" + senhaSEFAZ + ", identificacao=" + identificacao + ", dataAlteracao=" + dataAlteracao
				+ ", notas=" + Arrays.toString(notas) + "]";
	}
	
	




}
