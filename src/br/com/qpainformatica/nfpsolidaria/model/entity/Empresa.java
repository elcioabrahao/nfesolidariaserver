package br.com.qpainformatica.nfpsolidaria.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "empresa")
public class Empresa implements Serializable {

	@Id
	@Column(name = "id_empresa")
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonView(View.Summary.class)
	private int id;
	@Column(name = "codigo")
	@JsonView(View.Summary.class)
	public int codigo;
	@Column(name = "identificacao")
	@JsonView(View.Summary.class)
	public int identificacao;
	@Column(name = "nome")
	@JsonView(View.Summary.class)
	public String nome;
	@Column(name = "cnpj")
	@JsonView(View.Summary.class)
	public String cnpj;
	@Column(name = "data_alteracao")
	@JsonView(View.Summary.class)
	public String dataAlteracao;

	public Empresa() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(int identificacao) {
		this.identificacao = identificacao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getDataAlteracao() {
		return dataAlteracao;
	}

	public void setDataAlteracao(String dataAlteracao) {
		this.dataAlteracao = dataAlteracao;
	}

	@Override
	public String toString() {
		return "Empresa [id=" + id + ", codigo=" + codigo + ", identificacao=" + identificacao + ", nome=" + nome
				+ ", cnpj=" + cnpj + ", dataAlteracao=" + dataAlteracao + "]";
	}

	public Empresa(int id, int codigo, int identificacao, String nome, String cnpj, String dataAlteracao) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.identificacao = identificacao;
		this.nome = nome;
		this.cnpj = cnpj;
		this.dataAlteracao = dataAlteracao;
	}





}
