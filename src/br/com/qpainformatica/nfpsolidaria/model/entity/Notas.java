package br.com.qpainformatica.nfpsolidaria.model.entity;

import java.util.List;

public class Notas {

	public Notas() {
			}
	
	List<Nota> notas;

	public List<Nota> getNotas() {
		return notas;
	}

	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}

	
}
