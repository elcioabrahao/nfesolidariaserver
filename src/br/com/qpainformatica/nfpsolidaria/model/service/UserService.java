package br.com.qpainformatica.nfpsolidaria.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.nfpsolidaria.model.dao.UserDAO;
import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;
import br.com.qpainformatica.nfpsolidaria.model.entity.User;
import br.usjt.qpainformatica.nfpsolidaria.util.Util;

@Service
public class UserService {
	
	UserDAO dao;
	
	@Autowired
	public UserService(UserDAO dao){
		this.dao=dao;
	}

	public List<Nota> getNotas(User user){
		
		return dao.getUserNotas(user);
	}
	
	public User getUserByCPF(String cpf){
		
		cpf = Util.formatarString(cpf, "###.###.###-##");
		
		return dao.getUserByCPF(cpf);
	}
	
	public List<User> getAllUsers(){
		return dao.getAllUsers();
	}

	public List<User> getAllUsersMaiorQue(String data){
		return dao.getAllUsersMaiorQue(data);
	}
	


}
