package br.com.qpainformatica.nfpsolidaria.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.nfpsolidaria.model.dao.NotaDAO;
import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;
import br.usjt.qpainformatica.nfpsolidaria.util.Util;

@Service
public class NotaService {

	NotaDAO dao;
	
	@Autowired
	public NotaService(NotaDAO dao) {
		this.dao = dao;
	}
	
	public List<Nota> getAllNotas(){
		return dao.getAllNotas();
	}
	
	public List<Nota> getNotasByEmpresa(Integer id){
		
		return dao.getNotasByEmpresa(id);
	}
	
	public List<Nota> getNotasByEmpresaCNPJ(String cnpj){
		
		return dao.getNotasByEmpresaCNPJ(Util.formatarString(cnpj, "##.###.###/####-##"));
	}
	
	public List<Nota> getNotasByEmpresaCPF(Integer id,String cpf){
		
		return dao.getNotasByEmpresaCPF(id,Util.formatarString(cpf, "###.###.###-##"));
	}

	public List<Nota> getAllNotasMaiorQueData(String data){
		return dao.getAllNotasMaiorQueData(data);
	}
	
	public List<Nota> getAllNotasMaiorQueId(int id){
		return dao.getAllNotasMaiorQueId(id);
	}
	
	public String getImageNameById(int id){
		
		Nota nota = dao.getNotaById(id);
		
		return nota.getReferencia();
	}

	public Nota getNotaById(int id){
		return dao.getNotaById(id);
	}
	
	public List<Nota> getNotaByEmpresaMaiorQueData(String id, String data){
		return dao.getNotaByEmpresaMaiorQueData(id, data);
	}
	
	public List<Nota> getNotaByReferencia(String referencia){
		return dao.getNotaByReferencia(referencia);
	}
	
	public void alterarNotas(List<Nota> notas){
		dao.alterarNotas(notas);
	}
	
	public void deletarNotas(List<Nota> notas){
		dao.deletarNotas(notas);
	}
	
	public void deletarNotaById(int id){
		dao.deletarNotaById(id);
	}
	
}
