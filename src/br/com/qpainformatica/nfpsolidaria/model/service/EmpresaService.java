package br.com.qpainformatica.nfpsolidaria.model.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.qpainformatica.nfpsolidaria.model.dao.EmpresaDAO;
import br.com.qpainformatica.nfpsolidaria.model.entity.Empresa;
import br.usjt.qpainformatica.nfpsolidaria.util.Util;

@Service
public class EmpresaService {

	EmpresaDAO dao;
	
	@Autowired
	public EmpresaService(EmpresaDAO dao){
		this.dao=dao;
	}

	public List<Empresa> getEmpresasByCodigo(int codigo){
		return dao.getEmpresasByCodigo(codigo);
	}
	
	public Empresa getEmpresaByCNPJ(String cnpj){
		String cnpjFormatado="";
		if(cnpj.length()>14){
			cnpjFormatado = cnpj;
		}else{
			cnpjFormatado = Util.formatarString(cnpj, "##.###.###/####-##");
		}
		return dao.getEmpresaByCNPJ(cnpjFormatado);
	}
	
	public Empresa getEmpresaMaiorData(String data){
		return dao.getEmpresaMaiorData(data);
	}
	
	public int inserirAlterarEmpresa(Empresa empresa){
		try {
			return dao.inserirAlterarEmpresa(empresa);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
	
	public int alterarEmpresaByCodigo(int codigo, int identificacao){
		return dao.updateEmpresaByCodigo(codigo,identificacao);
	}
	
	

}
