package br.com.qpainformatica.nfpsolidaria.model.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;
import br.com.qpainformatica.nfpsolidaria.model.entity.User;

@Repository
public class UserDAO {
	
	@PersistenceContext
	EntityManager manager;

	public int inserirUser(User user) throws IOException {
		manager.persist(user);
		return user.getId();
	}
	
	public List<Nota> getUserNotas(User user){
		
		List<Nota> notas = new ArrayList<Nota>();
		
		System.out.println("Usuario no parâmetro: "+user.toString());
		
		List<User> users = manager.createQuery("select user from User user where cpf = '"+user.getCpf()+"'").getResultList();
		User userHere;
		if(users.size()>0){
			userHere = users.get(0);
			userHere.setCpf(user.getCpf());
			userHere.setDataNascimento(user.getDataNascimento());
			userHere.setNome(user.getNome());
			userHere.setSenhaSEFAZ(user.getSenhaSEFAZ());
			userHere.setIdentificacao(user.getIdentificacao());
			userHere.setDataAlteracao(user.getDataAlteracao());
			manager.merge(userHere);
		}else{
			userHere = new User();
			userHere.setId(0);
			userHere.setCpf(user.getCpf());
			userHere.setDataNascimento(user.getDataNascimento());
			userHere.setNome(user.getNome());
			userHere.setSenhaSEFAZ(user.getSenhaSEFAZ());
			userHere.setIdentificacao(user.getIdentificacao());
			userHere.setDataAlteracao(user.getDataAlteracao());
			manager.persist(userHere);
		}

		
		List<Nota> ln;
		Nota nn;
		for(Nota n: user.getNotas()){
			ln = manager.createQuery("select nota from Nota nota where referencia = '"+n.getReferencia()+"'").getResultList();
			if(ln.size()==0){
				n.setId(0);
				manager.persist(n);
			}else{
				nn = ln.get(0);
				if(n.getStatus()<2){
					n.setStatus(nn.getStatus());
					manager.merge(n);
				}
			}
		}
		
		notas = manager.createQuery("select nota from Nota nota where user_id = '"+user.getCpf()+"' and status >= 2").getResultList();
		
		return notas;
	}
	
	public User getUserByCPF(String cpf){
		
		List<User> users = manager.createQuery("select user from User user where cpf = '"+cpf+"'").getResultList();
		if(users.size()>0){
			return users.get(0);
		}else{
			return new User();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers(){
	    Query query = manager.createQuery("SELECT user FROM User user");
	    return (List<User>) query.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsersMaiorQue(String data){
	    Query query = manager.createQuery("SELECT user FROM User user where data_alteracao > '"+data+"'");
	    return (List<User>) query.getResultList();
		
	}
	
	


}
