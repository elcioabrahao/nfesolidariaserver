package br.com.qpainformatica.nfpsolidaria.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;

@Repository
public class NotaDAO {

	@PersistenceContext
	EntityManager manager;
	
	public List<Nota> getAllNotas(){
		return manager.createQuery("select nota from Nota nota").getResultList();
	}
	
	public List<Nota> getNotasByEmpresa(int id){
		return manager.createQuery("select nota from Nota nota where empresa='"+id+"'").getResultList();
	}
	
	public List<Nota> getNotasByEmpresaCNPJ(String cnpj){
		
		Query q = manager.createNativeQuery("SELECT n.* FROM nota n, empresa e WHERE n.empresa = e.codigo and e.cnpj=:cnpj", Nota.class);
		q.setParameter("cnpj", cnpj);
		return q.getResultList();
		 
	}
	
	public List<Nota> getNotasByEmpresaCPF(int id,String cpf){
		System.out.println("id="+id+" CPF="+cpf);
		return manager.createQuery("select nota from Nota nota where empresa='"+id+"' and user_id='"+cpf+"'").getResultList();
	}
	
	public List<Nota> getAllNotasMaiorQueData(String data){
		return manager.createQuery("select nota from Nota nota where data_referencia > '"+data+"'").getResultList();
	}
	
	public List<Nota> getAllNotasMaiorQueId(int id){
		return manager.createQuery("select nota from Nota nota where id_nota > "+id).getResultList();
	}
	

	public Nota getNotaById(int id){
		return manager.find(Nota.class, id);
	}
	
	public List<Nota> getNotaByEmpresaMaiorQueData(String id, String data){
		return manager.createQuery("select nota from Nota nota where data_referencia > '"+data+"' and empresa = '"+id+"'").getResultList();
	}

	public List<Nota> getNotaByReferencia(String referencia){
		return manager.createQuery("select nota from Nota nota where referencia = '"+referencia+"'").getResultList();
	}
	
	public void alterarNotas(List<Nota> notas){
		for(Nota nota: notas){
			manager.merge(nota);
		}
	}
	
	public void deletarNotas(List<Nota> notas){
		for(Nota nota: notas){
			manager.remove(manager.contains(nota) ? nota : manager.merge(nota));
		}
	}
	
	public void deletarNotaById(int id){
	
		Nota nota =  manager.find(Nota.class, id);
		if(nota != null){
			manager.remove(manager.contains(nota) ? nota : manager.merge(nota));
		}		
		
	}
	

}
