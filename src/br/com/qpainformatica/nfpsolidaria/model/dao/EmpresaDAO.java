package br.com.qpainformatica.nfpsolidaria.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.qpainformatica.nfpsolidaria.model.entity.Empresa;
import br.com.qpainformatica.nfpsolidaria.model.entity.Nota;
import br.com.qpainformatica.nfpsolidaria.model.entity.User;

@Repository
public class EmpresaDAO {
	
	@PersistenceContext
	EntityManager manager;
	
	public int inserirAlterarEmpresa(Empresa empresa) throws IOException {
		
		Empresa empresaLocal = null;
		List<Empresa> empresas = manager.createQuery("select empresa from Empresa empresa where cnpj = '"+empresa.getCnpj()+"'").getResultList();
		if(empresas.size()>0){
			empresaLocal = empresas.get(0);
			empresa.setId(empresaLocal.getId());
			manager.merge(empresa);
			return 2;
		}else{
			empresa.setId(0);
			manager.persist(empresa);
			return 1;
		}
	}
	
	public List<Empresa> getEmpresasByCodigo(int codigo){
		return manager.createQuery("select empresa from Empresa empresa where codigo='"+codigo+"'").getResultList();
	}
	
	public Empresa getEmpresaByCNPJ(String cnpj){
		List<Empresa> empresas = manager.createQuery("select empresa from Empresa empresa where cnpj='"+cnpj+"'").getResultList(); 
		if(empresas.size()>0){
			return empresas.get(0);
		}else{
			return new Empresa();
		}
	}
	
	public Empresa getEmpresaMaiorData(String data){
		List<Empresa> empresas = manager.createQuery("select empresa from Empresa empresa where data_alteracao > '"+data+"'").getResultList(); 
		if(empresas.size()>0){
			return empresas.get(0);
		}else{
			return new Empresa();
		}
	}

	public int updateEmpresaByCodigo(int codigo, int identificacao){
		Empresa empresa = this.getEmpresasByCodigo(codigo).get(0);
		if(empresa !=null){
			empresa.setIdentificacao(identificacao);
			manager.merge(empresa);
			return empresa.getId();
		}else{
			return 0;
		}
	}
}
