package br.usjt.qpainformatica.nfpsolidaria.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.text.MaskFormatter;

public class Util {

	public Util() {
		// TODO Auto-generated constructor stub
	}
	
	public static String formatarString(String texto, String mascara)  {
        String retorno="";
        try {
    		MaskFormatter mf = new MaskFormatter(mascara);
            mf.setValueContainsLiteralCharacters(false);
			retorno = mf.valueToString(texto);
		} catch (ParseException e) {
			retorno = "";
			e.printStackTrace();
		}
        
        return retorno;
    }
	
    public static String getFormatedDateLong(String data){

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        Date dt;
        try {
            dt = sdf.parse(data);
        } catch (ParseException e) {
            e.printStackTrace();
            return "Data Inválida";
        }

        SimpleDateFormat sdfOut = new SimpleDateFormat("yyyyMMddHHmmss");

        return sdfOut.format(dt);
    }
    
    public static String stripCNPJ(String cnpj){
    	return cnpj.replace(".", "").replaceAll("-", "").replaceAll("/", "");
    }

}
